.PHONY: main

main: main.cpp
	g++ -O3 main.cpp -lGL -lGLEW -lglfw -lGLU -lglut -o main.exe
	./main.exe
	rm main.exe